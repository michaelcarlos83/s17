console.log('Hello World');

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


/*printUsers();
let printFriends() =*/

function printUsers()
{
	alert("Hi! Please add the names of your friends.");
	let friend1 = /*alert*/prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friends = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friends); 
};

// console.log(friend1);
// console.log(friend2);
printUsers();



function userDetails(){
	
	alert('Please enter your details')
	let userFullName = prompt('Enter your fullname:')
	let userAge = prompt('Enter your Age:')
	let userLocation = prompt('Enter your location:')
	console.log('Hello, ' + userFullName + '\nYou are ' + userAge + ' years old.' + '\nYou live in ' + userLocation)
	alert('Thanks for your input')
}

userDetails();


function topMusic(){

	console.log('1. Coldplay' + '\n2. Maroon 5' + '\n3. Ben and Ben' + '\n4. Imagine Dragons' + '\n5. Eminem')
}

topMusic();


function topMovies(){
	console.log('1. Kimi no Nawa' + '\nRotten Tomatoes Rating: 98%' + '\n2. Avengers End Game' + '\nRotten Tomatoes Rating: 94%' + '\n3. John Wick' + '\nRotten Tomatoes Rating: 86%' + '\n4. Pursuit of Happyness' + '\nRotten Tomato Rating: 98%' + '\n5. Avatar (2009)' + '\nRotten Tomato Rating: 81%')
}

topMovies();

